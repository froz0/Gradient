# coding: utf-8
# *███████╗██████╗**██████╗*███████╗
# *██╔════╝██╔══██╗██╔═████╗╚══███╔╝
# *█████╗**██████╔╝██║██╔██║**███╔╝*
# *██╔══╝**██╔══██╗████╔╝██║*███╔╝**
# *██║*****██║**██║╚██████╔╝███████╗
# *╚═╝*****╚═╝**╚═╝*╚═════╝*╚══════╝
# Test file for gradient.py
import gradient
import json


def test_process_post_data():
    data = gradient.process_post_data(json.dumps({"test0": "test0"}).encode("utf_8"))
    assert data == {"test0": "test0"}


def test_process_post_data2():
    data = gradient.process_post_data(json.dumps({"test0": "test0", "test1": "test1"}).encode("utf_8"))
    assert data == {"test0": "test0", "test1": "test1"}

def test_process_post_data3():
    data = gradient.process_post_data(json.dumps({"test0": "test0", "test1": ["test1", "test1"]}).encode("utf_8"))
    assert data == {"test0": "test0", "test1": ["test1", "test1"]}
