"""
Gradient bot by froz, discord voice + web UI
███████╗██████╗**██████╗*███████╗
██╔════╝██╔══██╗██╔═████╗╚══███╔╝
█████╗**██████╔╝██║██╔██║**███╔╝*
██╔══╝**██╔══██╗████╔╝██║*███╔╝**
██║*****██║**██║╚██████╔╝███████╗
╚═╝*****╚═╝**╚═╝*╚═════╝*╚══════╝
"""
import os
import ssl
import json
from http.server import HTTPServer, BaseHTTPRequestHandler
import multiprocessing
from multiprocessing import Process
import discord
from discord.utils import get
from discord.ext import tasks
from discord import FFmpegPCMAudio  # pylint: disable=unused-import
from colored_logs.logger import Logger, LogType
import mysql.connector

# Var setup
manager = multiprocessing.Manager()
states = manager.dict()
actionQueue = manager.dict()
client = discord.Client()
voicer = {}
song_queue = manager.dict()
log = Logger(ID='setup-bot')


def process_post_data(data):
    """
    Process post data and output a dict
    """
    data = data.decode("utf-8")
    return json.loads(data)


def my_sql_connect():
    """
    Function to connect to SQL
    """
    return mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="Oasis780",
        database="gradient")


def after_song(guild):
    """
    This fonction is called when a song is skip , stop, finished
    """
    if len(song_queue[guild]) > 0:
        if song_queue[guild][0]["song_type"] == "mp3":
            play_song(guild, song_queue[guild][0]["id"])
            guild_queue = song_queue[guild]
            guild_queue.pop(0)
            song_queue[guild] = guild_queue
    else:
        guild_data = states[guild]
        guild_data["is_playing"] = 0
        states[guild] = guild_data
        guild_data["track"] = {
            "title": "Gradient",
            "artist": "Gradient",
            "cover": "/img/default-cover.png",
            "progress": 0,
            "song_length": 1,
            "id": -1,
            }
        states[guild] = guild_data


def play_song(guild, song_id):
    """
    Play song using the guild-id and the song id
    """
    sql_database = my_sql_connect()
    cursor = sql_database.cursor()
    cursor.execute("SELECT * FROM tracks WHERE id=" + str(song_id))
    data = cursor.fetchone()
    name = data[1]
    artist_id = data[2]
    cursor.execute("SELECT name FROM artists WHERE id=" + str(artist_id))
    data_artist = cursor.fetchone()
    artist = data_artist[0]
    image = data[4]
    length = data[6]
    if states[guild]["is_playing"] == 1:
        voicer[guild].stop()
    else:
        guild_data = states[guild]
        guild_data["is_playing"] = 1
        states[guild] = guild_data
    exec("""
    voicer[guild].play(discord.FFmpegPCMAudio(data[7]),after=lambda e: after_song('"+guild+"'))""")  # pylint: disable=exec-used
    data_state = states[guild]
    data_state["track"] = {
        "title": name,
        "artist": artist,
        "cover": image,
        "progress": 0,
        "song_length": length,
        "id": song_id,
        }
    states[guild] = data_state


def process_play_song(song_data, guild):
    """
    Fonction can play song or add it to the song queue
    """
    guild_queue = song_queue[guild]
    if song_data["play_position"] == "now":
        play_song(guild, song_data["song_id"])
    elif song_data["play_position"] == "after_after":
        guild_queue.append({
            "song_type": "mp3",
            "id": song_data["song_id"]
        })
    elif song_data["play_position"] == "after_now":
        guild_queue = song_queue[guild]
        guild_queue.insert(0, {
            "song_type": "mp3",
            "id": song_data["song_id"]
        })
    song_queue[guild] = guild_queue


@tasks.loop(seconds=1.0)
async def process_queue():
    """
    process the action queue every second,
    this allow communication between web and discord
    """
    for guild in actionQueue:
        for action in actionQueue[guild]:
            if action == "play":
                voicer[guild].resume()
            elif action == "pause":
                voicer[guild].pause()
            elif action == "play_song":
                process_play_song(actionQueue[guild][action], guild)
            elif action == "skip":
                voicer[guild].stop()
            elif action == "back":
                guild_queue = song_queue[guild]
                guild_queue.insert(0, {
                    "song_type": "mp3",
                    "id": states[guild]["track"]["id"]
                    })
                song_queue[guild] = guild_queue
                voicer[guild].stop()
    for guild in states:
        if states[guild]["is_playing"]:
            data_state = states[guild]
            data_track = data_state["track"]
            progress = int(data_track["progress"])
            data_track["progress"] = progress+1
            data_state["track"] = data_track
            states[guild] = data_state
    actionQueue.clear()


def load_file(path, placeholders):
    """
    Load file and remplace placeholders (Dict)
    """
    file = open(path, "r")
    txt = file.read()
    file.close()
    for placeholder in placeholders:
        txt = txt.replace(placeholder, placeholders[placeholder])
    return txt


def is_connected(guild):
    """
    Check if the bot is connected to the guild
    """
    voice_client = get(client.voice_clients, guild=guild)
    return voice_client and voice_client.is_connected()


def load_json(path):
    """
    Load json from files and make a dict
    """
    file = open(path, "r", encoding='utf-8')
    content = file.read()
    file.close()
    return json.loads(content)


@client.event
async def on_ready():
    """
    Called when the bot is ready
    """
    log.stop_process(log_type=LogType.Success, values='Gradient is online')


@client.event
async def on_message(message):
    """
    Happen when a message is caught by the bot
    """
    if message.author == client.user:
        return
    if message.content.lower().startswith("!i"):
        table = message.content.split()
        if len(table) > 1:
            if table[1] == "control":
                if not message.author.voice:
                    await message.channel.send(
                        f'{message.author.mention} You are not connected 😔'
                        )
                else:
                    voice_chat = await message.author.voice.channel.connect()
                    voicer[str(message.guild.id)] = voice_chat
                    chn = await message.author.create_dm()
                    gen_user_infos(message.author)
                    if str(message.channel.guild.id) not in states:
                        states[str(message.channel.guild.id)] = {
                            "track":
                            {
                                "title": "Gradient",
                                "artist": "Gradient",
                                "cover": "/img/default-cover.png",
                                "progress": 0,
                                "song_length": 1,
                                "id": -1},
                            "is_playing": 0,
                            "tune": 20
                            }
                        song_queue[str(message.channel.guild.id)] = []
                    await chn.send(
                        "Here is your link ! "
                        + "https://bot.gradientbot.tech/control?userid="
                        + str(message.author.id)
                        + "?guildid="
                        + str(message.channel.guild.id)
                        )
        else:
            await message.channel.send(f'Hi {message.author.mention}, if you want a control link !i control 😪')


def gen_user_infos(user):
    """
    Generate user info in the SQL database
    """
    sql_database = my_sql_connect()
    cursor = sql_database.cursor()
    cursor.execute("SELECT * FROM users WHERE `user-id`='"+str(user.id)+"'")
    if not cursor.fetchone():
        cursor.execute("INSERT INTO users (`user-id`, `avatar-link`, `name`) VALUES('"+str(user.id)+"', '"+str(user.avatar_url)+"', '"+user.name+"');")
        sql_database.commit()
    else:
        cursor.execute("UPDATE users SET `avatar-link`='"+str(user.avatar_url)+"', name='"+user.name+"' WHERE `user-id`='"+str(user.id)+"';")
        sql_database.commit()
    cursor.close()
    sql_database.close()


def add_action(guild, action_key, action_content):
    """
    Add action in the action queue
    """
    if guild not in actionQueue:
        actionQueue[guild] = {}
    action_data = actionQueue[guild]
    action_data[action_key] = action_content
    actionQueue[guild] = action_data


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    """
    HTTP server engine allow bot to have a cool web UI
    """
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):  # pylint: disable=invalid-name
        """
        Handle GET request on the http server
        """
        if "control?userid=" in self.path:
            ids = self.path.replace("/control?userid=", "").replace("guildid=", "")
            ids_table = ids.split("?")
            try:
                sql_database = my_sql_connect()
                cursor = sql_database.cursor()
                cursor.execute("SELECT * FROM users WHERE `user-id`="+ids_table[0]+"")
                data = cursor.fetchone()
                cursor.close()
                sql_database.close()
                reponse = load_file(os.getcwd()+"/static/home.html", {
                    "[name]": data[3],
                    "[avatar_url]": data[2],
                    "[debug]": str(states[ids_table[1]]),
                    "[guild-id]": ids_table[1],
                    "[user-id]": ids_table[0]
                    })
                self._set_response()
                self.wfile.write(reponse.encode("utf_8"))
            except Exception as ex:  # pylint: disable=broad-except
                log.error(ex)
                self.send_response(404)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                reponse = load_file(os.getcwd()+"/static/404.html", {})
                self.wfile.write(reponse.encode("utf_8"))
        else:
            try:
                content_types = {
                    ".html": "text/html",
                    ".png": "image/png",
                    ".gif": "image/gif",
                    ".js": "application/javascript",
                    ".jpg": "image/jpeg",
                    ".css": "text/css",
                    ".jpeg": "image/jpeg",
                    ".json": "application/json"
                    }
                file = open(os.getcwd()+"/www"+self.path, mode='rb')
                file_content = file.read()
                self.send_response(200)
                use_def = True
                for content_type in content_types:
                    if self.path.endswith(content_type):
                        use_def = False
                        self.send_header("Content-type", content_types[content_type])
                if use_def:
                    self.send_header("Content-type", "text/html")
                self.end_headers()
                self.wfile.write(file_content)
            except Exception as ex:  # pylint: disable=broad-except
                log.error(str(ex))
                self.send_response(404)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                reponse = load_file(os.getcwd()+"/static/404.html", {})
                self.wfile.write(reponse.encode("utf_8"))

    def do_POST(self):  # pylint: disable=invalid-name
        """
        Handle POST request on the http server
        """
        reply = ""
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        data = process_post_data(post_data)
        if data["task"] == "state":
            reply = json.dumps(states[data["guild"]])
        elif data["task"] == "play":
            if states[data["guild"]]["is_playing"] == 1:
                guild_data = states[data["guild"]]
                guild_data["is_playing"] = 0
                states[data["guild"]] = guild_data
                add_action(data["guild"], "pause", {})
            else:
                guild_data = states[data["guild"]]
                guild_data["is_playing"] = 1
                states[data["guild"]] = guild_data
                add_action(data["guild"], "play", {})
        elif data["task"] == "play_song":
            if data["play_type"] == "mp3":
                add_action(data["guild"], "play_song", {
                    "play_type": data["play_type"],
                    "song_id": data["song_id"],
                    "play_position": data["play_position"]
                    })
        elif data["task"] == "skip":
            add_action(data["guild"], "skip", {})
        elif data["task"] == "back":
            add_action(data["guild"], "back", {})
        elif data["task"] == "getqueue":
            queue_guild = song_queue[data["guild"]]
            to_json = []
            for song in queue_guild:
                if song["song_type"] == "mp3":
                    sql_database = my_sql_connect()
                    cursor = sql_database.cursor()
                    cursor.execute("SELECT * FROM tracks WHERE id="+str(song["id"]))
                    data = cursor.fetchone()
                    artist_id = data[2]
                    cursor.execute("SELECT name FROM artists WHERE id="+str(artist_id))
                    data_artist = cursor.fetchone()
                    cursor.close()
                    sql_database.close()
                    to_json.append({"name": data[1], "artist": data_artist[0], "image": data[4], "id": song["id"], "type": "mp3"})
            reply = json.dumps(to_json)
        elif data["task"] == "setqueue":
            song_queue[data["guild"]] = data["queue"]
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(reply.encode('utf-8'))

    def log_message(self, format, *args):  # pylint: disable=redefined-builtin
        """
        removing message log at every connexion
        """
        return


def webserv():
    """
    Launch the http service with SSL
    """
    httpd = HTTPServer(('0.0.0.0', 443), SimpleHTTPRequestHandler)
    ssl_key = "/etc/letsencrypt/live/bot.gradientbot.tech/privkey.pem"
    ssl_cert = "/etc/letsencrypt/live/bot.gradientbot.tech/fullchain.pem"
    httpd.socket = ssl.wrap_socket(
        httpd.socket,
        keyfile=ssl_key,
        certfile=ssl_cert,
        server_side=True
        )
    httpd.serve_forever()


def discordserv():
    """
    Launch discord service
    """
    try:
        log.ID = 'discord-api'
        log.start_process('Starting Discord services')
        process_queue.start()
        client.run('Njg0ODc5NTEzOTk3NjcyNDg4.XmAqdg.52QZjPyCkrQqgeHDK9z-LTkrgXc')
    except Exception as error:  # pylint: disable=broad-except
        log.critical("Something bad happened :"+str(error))


def main():
    """
    Main function launched if main prog
    """
    print('███████╗██████╗  ██████╗ ███████╗')
    print('██╔════╝██╔══██╗██╔═████╗╚══███╔╝')
    print('█████╗  ██████╔╝██║██╔██║  ███╔╝ ')
    print('██╔══╝  ██╔══██╗████╔╝██║ ███╔╝  ')
    print('██║     ██║  ██║╚██████╔╝███████╗')
    print('╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚══════╝')
    log.start_process('Connecting to MySQL..')
    log.ID = 'web-server'
    Process(target=webserv).start()
    log.success("Web server started")
    log.ID = 'discord-api'
    discordserv()


if __name__ == "__main__":
    main()
